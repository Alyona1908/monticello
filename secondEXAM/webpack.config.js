const path = require("path");
const webpack = require("webpack");
const webpack_rules = [];
const webpackOption = {
    mode: 'development',
    entry: "./src/app.js",
    output: {
        path: path.resolve(__dirname, "dev"),
        filename: "app.min.js",
    },
    module: {
        rules: webpack_rules
    }
};
module.exports = webpackOption;
